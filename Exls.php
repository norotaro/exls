<?php

class Exls extends CWidget
{
	private $_debug		= false;

	public $fileName	= 'file';
	public $fillRows	= true;
	public $columns;
	public $head;
	public $datas;

	public function run()
	{
		$table = CHtml::tag(
			'table',
			array(
			'cellpading'	=> 2,
			'style'			=> '
				border-spacing:1px;
				font:11px Verdana, Arial, Helvetica, sans-serif;
				border:1px solid;',
			),
			$this->_render(),
			true
		);

		if( $this->_debug )
			echo $table;
		else
			Yii::app()->request->sendFile($this->fileName.'.xls', $table);

		Yii::app()->end();
	}

	protected function _render()
	{
		$render = $this->_renderHead();
		$render .= $this->_renderBody();
		$render .= $this->_renderFoot();

		return $render;
	}

	protected function _renderHead()
	{
		if(empty($this->head)) return '';

		$options = isset($this->head['options']) ? $this->head['options'] : array();
		unset($this->head['options']);

		$head = '';
		foreach ($this->head as $data) {
			$head .= $this->_row(array('options' => $options, $data));
		}

		return CHtml::tag('thead', array(), $head, true);
	}

	protected function _renderBody()
	{
		//Render columns names
		$body = ($columns = $this->columns) ?
			$this->_row(array_merge(
				array('options'=>array('style'=>'background-color:#999')), $columns
			), 'th') : '';

		if(empty($this->datas)) return $body;

		$fill = false;
		$globOptions = isset($this->datas['options']) ? $this->datas['options'] : array();
		unset($this->datas['options']);

		foreach ($this->datas as $data) {
			$options = $fill && $this->fillRows ?
				array_merge(array('style'=>'background-color:#DDD'), $globOptions) : $globOptions;
			$body .= $this->_row(array_merge(array('options'=>$options), $data));
			$fill = !$fill;
		}

		return $body;
	}

	protected function _renderFoot()
	{
		if(empty($this->foot)) return '';

		$options = isset($this->foot['options']) ? $this->foot['options'] : array();
		unset($this->foot['options']);

		$foot = '';
		foreach ($this->foot as $data) {
			$foot .= $this->_row(array('options' => $options, $data));
		}

		return CHtml::tag('tfoot', array(), $foot, true);
	}

	/**
	 * $columns = array(
	 * 		'options' => array(default options), //optional
	 * 		'columnName1'=>array(options),
	 * 		'columnName2'
	 * )
	 */
	protected function _row($columns, $cType = 'td')
	{
		$options = array(
			'height'	=> 30,
		);
		if( isset($columns['options']) )
			$options = array_merge($options, $columns['options']);
		unset($columns['options']);

		$row = '';
		foreach ($columns as $k => $val) {
			if( is_array($val) ) {
				$opts = array_merge($options, $val);
				$content = $k;
			} else {
				$content = $val || empty($options['empty']) ? $val : $options['empty'];
				$opts = $options;
			}
			unset($opts['empty']);

			$row .= CHtml::tag($cType, $opts, $content, true);
		}

		return CHtml::tag('tr', array(), $row, true);
	}
}